#!/bin/bash
# Fecha: 17-08-2022
# Autor: Fatima Azucena MC
# Correo: fatimaazucenamartinez274@gmail.com

titulos=("Unidad 1","Unidad 2","Unidad 3","Unidad 4","Unidad 5")
calificaciones=()

for (( i = 0; i < 5; i++ )) do
	echo "Ingrese el número en la posición  "$i
	read calificacion
	calificaciones[$i]=$calificacion
	sumatoria=$(($sumatoria+${calificaciones[$i]}))
done
promedio=$(($sumatoria/5))
echo "El promedio es de: " $promedio
